#!/usr/bin/python

'''
by shivam chudasama - may 2013 
This program reads data coming from the serial port and saves that data to a text file. It expects data in the format:
"photocell_reading,thermistor_reading"

It assumes that the Arduino shows up in /dev/ttyACM0 on the Raspberry Pi which should happen if you're using Debian.
'''


import serial
ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=0.5)
ser.write('*99C\r\n') 

try:
	while 1:
        
        
        data = ser.read(7)
            mdata=struct.unpack('>hhhc', data)
            s1=mdata[0]
            s2=mdata[1]
            s3=mdata[2]
            r1=s1+s2+s3
            b2= (s1**2)      # carre
            c2= (s2**2)      # carre
            d2= (s3**2)      # carre
            t=b2+c2+d2
            r=sqrt(t)   #racine carre
            r2=int(round(r))
        
        data = [date2,time2, lat, long, speed,Longerror,Laterror,track,s1, s2, s3,r1,r2]  
        
		
		temp2=r2
		print("%s"%(temp2))
		f=open('tempLog.dat','a')
		print >>f,("%s"%(temp2))
		f.close()
except KeyboardInterrupt:
	print "\ndone"
