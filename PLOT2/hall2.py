import RPi.GPIO as GPIO
import os
from gps import *
import time
import threading
import sqlite3
import signal
from datetime import date
#from datetime import time
from datetime import datetime
#import csv
import sys
import serial
import struct
from math import sqrt


GPIO.setmode(GPIO.BCM)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP)


gpsd = None #seting the global variable
#os.system('clear') #clear the terminal (optional)
 
class GpsPoller(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    global gpsd #bring it in scope
    gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
    self.current_value = None
    self.running = True #setting the thread running to true
 
  def run(self):
    global gpsd
    while gpsp.running:
      gpsd.next() #this will continue to loop and grab EACH set of gpsd info to clear the buffer


def sercmd(cmd, resp):
    done=0
    while (done==0):
        ser.write(cmd)
        time.sleep(0.1)
        data=ser.read(len(resp))
        #print data
        if (data == resp):
            done = 1
        else:
             #Send an Esc just in case it was streaming
            ser.write(chr(int('1b', 16)))
            ser.read(100)

ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=0.5)
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99A\r\n', 'ASCII ON\r')   #99B BINARY ON      99A  ASCII ON
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99ZF\r\n', 'ZERO OFF\r')
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99ZN\r\n', 'ZERO ON\r')
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99R=10\r\n', 'OK\r')       #Change to 10 bug si inferireur
ser.write('*99C\r\n')               #Output stream x, y, z stream} P=Polled - Output a single sample. C=Continuous







if __name__ == '__main__':
    gpsp = GpsPoller() # create the thread
   
    #bgps = open('resultgps.csv', 'w')
    #bsense = open('sense.csv', 'a')
    
    conn = sqlite3.connect('/usr/share/adafruit/webide/repositories/my-pi-projects/Final/final.db')
    #conn = sqlite3.connect('gpsLog.db')
    cur = conn.cursor()
   
    
    try:
        gpsp.start() # start it up
        while True:
            os.system('clear')
            today = date.today()
            #timos = datetime.time(datetime.now())
                        
            data = ser.read(7)
            mdata=struct.unpack('>hhhc', data)
            s1=mdata[0]
            s2=mdata[1]
            s3=mdata[2]
            r1=s1+s2+s3
            b2= (s1**2)      # carre
            c2= (s2**2)      # carre
            d2= (s3**2)      # carre
            t=b2+c2+d2
            r=sqrt(t)   #racine carre
            r2=int(round(r))
            
            lat = gpsd.fix.latitude
            long = gpsd.fix.longitude
            speed = gpsd.fix.speed
            Longerror = gpsd.fix.epx
            Laterror = gpsd.fix.epy
            track = gpsd.fix.track    #Course over ground, degrees from true north.
            timegps1 = gpsd.utc
            timegps2 = gpsd.fix.time
            
            now = datetime.now()
                        
            date2 = now.strftime('%d/%m/%Y')
            time2 = now.strftime('%H:%M:%S')
                       
            print 'latitude    ' , gpsd.fix.latitude
            print 'longitude   ' , gpsd.fix.longitude
            print 'magnet      ' , r2
            print 'time        ' ,time2
            print 'timegps1    ' ,timegps1
            print 'timegps2    ' ,timegps2
           
           
            #print 'time' , timos
                        
            #a = csv.writer(bsense)
            #data2 = [[today,timos,lat,long,speed,Longerror,Laterror,track,s1, s2, s3,r1,r2]]
            #a.writerows(data2)
            
            input_state = GPIO.input(24)
            print 'goio input state 24', input_state
            
            
            data = [date2,time2, lat, long, speed,Longerror,Laterror,track,s1, s2, s3,r1,r2]  
                
            cur.execute("INSERT INTO sensorgps (date,time,latitude,longitude,speed,Longerror,Laterror,track,s1,s2,s3,r1,r2) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", data)
            
            conn.commit()
            
            if input_state == False:
                print('Button shutdown Pressed')
                os.system("sudo shutdown -h now")
                time.sleep(0.2)    
           
          
            #def Int_shutdown(channel):  
            #    os.system("sudo shutdown -h now")
    
            #GPIO.add_event_detect(24, GPIO.FALLING, callback = Int_shutdown, bouncetime = 2000)   
            
            #data = [today,today, lat, long, speed,Longerror,Laterror,track,s1, s2, s3,r1,r2]
            #cur.execute("INSERT INTO gpsdata ( time, date, latitude, longitude,speed,Longerror,Laterror,track,s1, s2, s3,r1,r2) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", data)
            #conn.commit()
            
    
            #db = MySQLdb.connect("localhost", "root", "fghj", "test")
            #db = MySQLdb.connect("localhost", "root", "fghj", "test")
            #cursmysql=db.cursor()

            #db = MySQLdb.connect("localhost", "phil", "fghj", "test")
            #curs=db.cursor()


            #db = MySQLdb.connect(host="localhost", # your host, usually localhost
            #                     user="john", # your username
            #                      passwd="megajonhy", # your passwor
            #                      db="jonhydb") # name of the data base





# you must create a Cursor object. It will let
#  you execute all the queries you need


            
            
            
            
            #input_state = GPIO.input(24)

            
            
            
            time.sleep(0.5) #set to whatever
    except (KeyboardInterrupt): #when you press ctrl+c
    #except (KeyboardInterrupt,SystemExit,input_state): #when you press ctrl+c
    #except (KeyboardInterrupt,SystemExit,signal.signal(signal.TERM)): #when you press ctrl+c
        print "\nKilling Thread..."
        gpsp.running = False
        gpsp.join() # wait for the thread to finish what it's doing
        conn.close()  #Fermer base de donner
        #bgps.close()
        
        ser.write(chr(int('1b', 16)))
        time.sleep(0.25)
        ser.read(100)
        #bsense.close()  
        ser.close() #Fermer hmr2200
        
        print "Done.\nExiting."
        
    
    #except (GPIO.input(24) == GPIO.LOW): #when push button 24
        
    #    print "\nKilling Thread with push button..."
    #    gpsp.running = False
    #    gpsp.join() # wait for the thread to finish what it's doing
    #    conn.close()  #Fermer base de donner
    #    #bgps.close()
        
    #    ser.write(chr(int('1b', 16)))
    #    time.sleep(0.25)
    #    ser.read(100)
    #    #bsense.close()  
    #    ser.close() #Fermer hmr2200
    #    GPIO.cleanup()
    #    print "Done.\nExiting."
        
        
             
