import pandas as pd
import numpy as np

n = 100
idx = pd.date_range( start=dt.datetime.now( ), periods=n, freq='S' )
ts1= pd.Series( np.sin( np.linspace( 0, 4 * np.pi, n ) ), index=idx)
ts2= pd.Series( np.cos( np.linspace( 0, 4 * np.pi, n ) ), index=idx)

fig = plt.figure( figsize=(8, 6) )
ax = fig.add_axes( [.05, .05, .9, .9] )

ts1.plot( ax )
ts2.plot( ax )
(ts1 - ts2).plot( ax )