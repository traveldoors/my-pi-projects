import numpy as np
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib.pyplot as plt

#import data
data = np.loadtxt('DHT11.csv', delimiter = ',', skiprows = 1,
                     usecols = (0,2,3,4,5,6), unpack = 1)

#id, unixtime, temp, humidity, lightlevel, lightvolt

i = data[0]
time = data[1]
temp = data[2]
hum = data[3]
light_lv =  1/data[4]*1000
light_v = data[5]


if 1:

    host = host_subplot(111, axes_class=AA.Axes)
    plt.subplots_adjust(right=0.75)

    par1 = host.twinx()
    par2 = host.twinx()

    offset = 60
    new_fixed_axis = par2.get_grid_helper().new_fixed_axis
    par2.axis["right"] = new_fixed_axis(loc="right",
                                        axes=par2,
                                        offset=(offset, 0))

    par2.axis["right"].toggle(all=True)



    #host.set_xlim(0, 2)
    #host.set_ylim(0, 2)

    host.set_xlabel("Time (unix)")
    host.set_ylabel("Temperature (C)")
    par1.set_ylabel("Humidity (%)")
    par2.set_ylabel("Light (A.U.)")

    p1, = host.plot(time, temp)
    p2, = par1.plot(time, hum)
    p3, = par2.plot(time, light_lv)

    #par1.set_ylim(0, 4)
    #par2.set_ylim(1, 65)

    host.legend()

    host.axis["left"].label.set_color(p1.get_color())
    par1.axis["right"].label.set_color(p2.get_color())
    par2.axis["right"].label.set_color(p3.get_color())

    plt.draw()
    plt.show()