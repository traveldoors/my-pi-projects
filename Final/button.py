import RPi.GPIO as GPIO
import time
import os

GPIO.setmode(GPIO.BCM)

GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
        
    input_state = GPIO.input(16)
    if input_state == False:
        print('Button start')
        #os.system("sudo python /usr/share/adafruit/webide/repositories/my-pi-projects/Final/hall2.py")
        os.system("sudo python /usr/share/adafruit/webide/repositories/my-pi-projects/LAB-PI/COMPIL-ok4.py")
        time.sleep(0.2)
        
  
    input_state = GPIO.input(20)
    if input_state == False:
        print('Button shutdown Pressed')
        os.system("sudo shutdown -h now")
        time.sleep(0.2)    



def Int_shutdown(channel):  
	os.system("sudo shutdown -h now")
	
# Now we are programming pin 31 as an interrupt input
# it will react on a falling edge and call our interrupt routine "Int_shutdown"
GPIO.add_event_detect(20, GPIO.FALLING, callback = Int_shutdown, bouncetime = 2000)   

# do nothing while waiting for button to be pressed
while 1:
        time.sleep(1)