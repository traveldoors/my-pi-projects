import RPi.GPIO as GPIO
import time
import os

GPIO.setmode(GPIO.BCM)

GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        

        
def Int_Login(channel):  
    os.system("sudo python /usr/share/adafruit/webide/repositories/my-pi-projects/Final/hall2.py")

def Int_shutdown(channel):  
    os.system("sudo shutdown -h now")
	
# Now we are programming pin 31 as an interrupt input
# it will react on a falling edge and call our interrupt routine "Int_shutdown"
GPIO.add_event_detect(18, GPIO.FALLING, callback = Int_shutdown, bouncetime = 2000)   

GPIO.add_event_detect(23, GPIO.FALLING, callback = Int_Login, bouncetime = 2000)   

# do nothing while waiting for button to be pressed
while 1:
        time.sleep(1)