import sys
import serial
import time
import struct
import signal
import csv
from math import sqrt

def sercmd(cmd, resp):
    done=0
    while (done==0):
        ser.write(cmd)
        time.sleep(0.1)
        data=ser.read(len(resp))
        #print data
        if (data == resp):
            done = 1
        else:
             #Send an Esc just in case it was streaming
            ser.write(chr(int('1b', 16)))
            ser.read(100)

ser = serial.Serial('/dev/ttyUSB1', 9600, timeout=0.5)
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99A\r\n', 'ASCII ON\r')   #99B BINARY ON      99A  ASCII ON
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99ZF\r\n', 'ZERO OFF\r')
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99ZN\r\n', 'ZERO ON\r')
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99R=10\r\n', 'OK\r')       #Change to 10 bug si inferireur
ser.write('*99C\r\n')               #Output stream x, y, z stream} P=Polled - Output a single sample. C=Continuous

bsense = open('sense.csv', 'w')

time.sleep(0.1)
for i in range(2000):
    data = ser.read(7)
    mdata=struct.unpack('>hhhc', data)
    a0=mdata[0]
    b0=mdata[1]
    c0=mdata[2]
    a4=a0+b0+c0
    b2= (a0**2)      # carre
    c2= (b0**2)      # carre
    d2= (c0**2)      # carre
    t=b2+c2+d2
    r=sqrt(t)   #racine carre
    r2=int(round(r))

    print mdata[0], '\t', mdata[1], '\t', mdata[2],'\t',a4,'\t', r2#,'\t'#, c1,'\t', d1 #,'\t', t,'\t', r
    a = csv.writer(bsense)
    data2 = [[mdata[0], mdata[1], mdata[2],a4,r2]]      
    a.writerows(data2)

# Stop streaming
ser.write(chr(int('1b', 16)))
time.sleep(0.25)
ser.read(100)
bsense.close()
ser.close()