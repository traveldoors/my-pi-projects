
#http://blog.jacobean.net/?p=678

import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
from matplotlib import pyplot
from matplotlib.dates import date2num
import matplotlib.dates as mdates
import sqlite3
 
# Data series
speed = []
r1 = []
r2 = []
 
# Various formatters
# These will have to change as the volume of data to be plotted increases
# As the x-axis will become too cluttered
years    = mdates.YearLocator()   # every year
months   = mdates.MonthLocator()  # every month
days    = mdates.DayLocator()
hours   = mdates.HourLocator()
daysFmt = mdates.DateFormatter('%a %d %b %y')
 
# Connect to database
#conn = sqlite3.connect("/var/local/log/test.db", detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
conn = sqlite3.connect("/usr/share/adafruit/webide/repositories/my-pi-projects/Final/final.db", detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
 
 
 
# Set up SQL query and perform on the database
# Very poorly written 'exception' handling
# sql = 'SELECT time as "[timestamp]", r1, r2 FROM sensorgps'
sql = 'SELECT speed , r1, r2 FROM sensorgps'
conn.row_factory = sqlite3.Row
cursor = conn.cursor()
try:
    cursor.execute(sql)
    rows = cursor.fetchall()
finally:
    cursor.close()
 
# Transfer SQL data into the data series
# Must be a better way to do this
for row in rows:
    speed.append(row[0])
    r1.append(row[1])
    r2.append(row[2])
 
# Convert the dates into a format that matplotlib can handle
#dates = date2num(speed)
 
# Create the figure and 2 sublots
fig = pyplot.figure()
plt0 = fig.add_subplot(111)
plt1 = fig.add_subplot(211)
 
# Hide all un-necessary adornments
plt0.spines['top'].set_color('none')
plt0.spines['bottom'].set_color('none')
plt0.spines['left'].set_color('none')
plt0.spines['right'].set_color('none')
plt0.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
 
# Plot Temperature sub-plot
plt1.plot_date(speed, r1, 'b-', xdate=True)
plt1.set_title("r1")

plt1.xaxis.set_major_locator(days)
plt1.xaxis.set_major_formatter(daysFmt)
plt1.set_ylabel('hpa')
 
# Plot Pressure sub-plot
plt2 = fig.add_subplot(212)
plt2.plot_date(speed, r2, 'g-', xdate=True)
plt2.set_title("r2")
plt2.set_ylabel('hPa')
 
# Set x-axis label & format
plt2.xaxis.set_major_locator(days)
plt2.xaxis.set_major_formatter(daysFmt)
plt2.set_xlabel('Date')
fig.autofmt_xdate()
 
# Save the the graph image
pyplot.savefig('weather-plot2.png')
pyplot.show()
#- See more at: http://blog.jacobean.net/?p=678#sthash.F9ugKg4d.dpuf