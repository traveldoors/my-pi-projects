import sys
import serial
import time
import struct
import signal

def sercmd(cmd, resp):
    done=0
    while (done==0):
        ser.write(cmd)
        time.sleep(0.1)
        data=ser.read(len(resp))
        #print data
        if (data == resp):
            done = 1
        else:
            # Send an Esc just in case it was streaming
            ser.write(chr(int('1b', 16)))
            ser.read(100)
            
     

ser = serial.Serial('/dev/ttyUSB1', 9600, timeout=0.5)


# Set binary mode
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99B\r\n', 'BINARY ON\r')
# sercmd('*99A\r\n', 'ASCII ON\r')


# Set the zero mark
sercmd('*99WE\r\n', 'OK\r')            
sercmd('*99ZF\r\n', 'ZERO OFF\r')       
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99ZN\r\n', 'ZERO ON\r')


# Set average on
#sercmd('*99WE\r\n', 'OK\r')            
#sercmd('*99VF\r\n', 'AVG OFF\r')       
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99VN\r\n', 'AVG ON\r')


# Set sampling rate      Set sample rate to nnn where: nnn= 10,20,25,30, 40,50, 60, 100, 123, or 154 samples/sec (default=20 sps)
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99R=10\r\n', 'OK\r')

ser.write('*99C\r\n')
time.sleep(0.1)
for i in range(3000):
    data = ser.read(7)
    mdata=struct.unpack('>hhhc', data)
    print mdata[0], '\t', mdata[1], '\t', mdata[2]

# Stop streaming
ser.write(chr(int('1b', 16)))
time.sleep(0.25)
ser.read(100)

# Set ascii mode
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99A\r\n', 'ASCII ON\r')

# Close serial port
ser.close()



