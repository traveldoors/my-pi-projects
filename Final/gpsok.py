import os
from gps import *
import time
import threading
import sqlite3
import signal
from datetime import date
from datetime import time
from datetime import datetime
import csv

gpsd = None #seting the global variable
os.system('clear') #clear the terminal (optional)
 
class GpsPoller(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    global gpsd #bring it in scope
    gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
    self.current_value = None
    self.running = True #setting the thread running to true
 
  def run(self):
    global gpsd
    while gpsp.running:
      gpsd.next() #this will continue to loop and grab EACH set of gpsd info to clear the buffer
 
if __name__ == '__main__':
    gpsp = GpsPoller() # create the thread
    conn = sqlite3.connect('gpsLog.db')
    cur = conn.cursor()
    bgps = open('resultgps.csv', 'w')
    try:
        gpsp.start() # start it up
        while True:
            os.system('clear')
            print 'latitude    ' , gpsd.fix.latitude
            print 'longitude   ' , gpsd.fix.longitude
            today = date.today()
            print "Today's date is ", today
            timos = datetime.time(datetime.now())
            print "The current time is ", timos
            lat = gpsd.fix.latitude
            long = gpsd.fix.longitude
            data = [today,today, lat, long]
            print data
            cur.execute("INSERT INTO gpsdata ( time, date, latitude, longitude) VALUES( ?, ?, ?, ?)", data)
            conn.commit()
            a = csv.writer(bgps)
            data2 = [[timos, today, gpsd.fix.latitude, gpsd.fix.longitude]]      
            a.writerows(data2)
                    
        time.sleep(5) #set to whatever

    except (KeyboardInterrupt): #when you press ctrl+c
    #except (KeyboardInterrupt,SystemExit,signal.signal(signal.TERM)): #when you press ctrl+c
        print "\nKilling Thread..."
        gpsp.running = False
        gpsp.join() # wait for the thread to finish what it's doing
        conn.close()
        bgps.close()
        print "Done.\nExiting."
