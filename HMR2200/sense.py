import sys
import serial
import time
import struct
import signal
import csv
from math import sqrt

def sercmd(cmd, resp):
    done=0
    while (done==0):
        ser.write(cmd)
        time.sleep(0.1)
        data=ser.read(len(resp))
        #print data
        if (data == resp):
            done = 1
        else:
             #Send an Esc just in case it was streaming
            ser.write(chr(int('1b', 16)))
            ser.read(100)

ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=0.5)

# Set reset  ne marche pas
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99TN\r\n', 'S/R OFF\r')
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99TF\r\n', 'S/R ON\r')

# Set reset pulse ne marche pas
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99R\r\n', 'RST\r')
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99S\r\n', 'SET\r')

# Set binary mode
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99B\r\n', 'BINARY ON\r')   #99B BINARY ON      99A  ASCII ON
#sercmd('*99A\r\n', 'ASCII ON\r')   #99B BINARY ON      99A  ASCII ON

# Set the zero mark
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99ZF\r\n', 'ZERO OFF\r')
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99ZN\r\n', 'ZERO ON\r')

# Set the highest sampling rate
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99R=10\r\n', 'OK\r')       #Change to 10 bug si inferireur

# Set Average reading
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99VF\r\n', 'AVG OFF\r')
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99VN\r\n', 'AVG ON\r')

ser.write('*99C\r\n')               #Output stream x, y, z stream} P=Polled - Output a single sample. C=Continuous
#ser.write('*99P\r\n')               #Output stream x, y, z stream} P=Polled - Output a single sample. C=Continuous

b = open('hmr23002.csv', 'a')

time.sleep(0.1)
for i in range(50):
    data = ser.read(7)
    mdata=struct.unpack('>hhhc', data)
    
    #print(b1)
    #print(c1)
    #print(d1)
    #a3=mdata[0]+mdata[1]+mdata[2]
  

    a0=mdata[0]
    b0=mdata[1]
    c0=mdata[2]
    
    a4=a0+b0+c0
    #b1=int(mdata[0], 16)      #int(hexString, 16)  
    #c1=int(mdata[1], 16)      #int(hexString, 16)  
    #d1=int(mdata[2], 16)      #int(hexString, 16)  
    
    #a01=str(a0)
    #b01=str(a0)
    #c01=str(a0)
    
    #b1=int(a01, 16)      #int(hexString, 16)  
    #c1=int(b01, 16)      #int(hexString, 16)  
    #d1=int(c01, 16)      #int(hexString, 16)  


    b2= (a0**2)      # carre
    #print (b3) 
    c2= (b0**2)      # carre
    #print (c3) 
    d2= (c0**2)      # carre
    #print (d3) 

    t=b2+c2+d2
    #print (t) 
    r=sqrt(t)   #racine carre
    r2=int(round(r))
    #int(r)
    #print int(r)

    
    
    
    
    print mdata[0], '\t', mdata[1], '\t', mdata[2],'\t',a4,'\t', r2#,'\t'#, c1,'\t', d1 #,'\t', t,'\t', r
    a = csv.writer(b)
    data2 = [[mdata[0], mdata[1], mdata[2],a4,r2]]      
    a.writerows(data2)





# Stop streaming
ser.write(chr(int('1b', 16)))
time.sleep(0.25)
ser.read(100)

# Set ascii mode
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99A\r\n', 'ASCII ON\r')

# Close CVS
b.close()
# Close serial port
ser.close()



