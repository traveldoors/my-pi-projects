#! /usr/bin/python
# Written by Dan Mandle http://dan.mandle.me September 2012
# Modified by Peter Nichols for output to kml file http://www.itdiscovery.info
# License: GPL 2.0

import os
from gps import *
from time import *
import time
import threading
from math import radians, cos, sin, asin, sqrt

# LED Hooked to Pin 11
#import RPi.GPIO as GPIO
#GPIO.setmode(GPIO.BOARD)
#GPIO.setwarnings(False)
#GPIO.setup(11, GPIO.OUT, initial=GPIO.LOW)

# Arguments [-D debug-level] [-j interval] [-i track timeout]
# [-m minimum distance] [-c color of line] [-p color of polygon]

def haversine(lon1, lat1, lon2, lat2):
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    km = 6367 * c
    return km

slpinterval = 5
dbug = 0
datestmp = time.strftime("%Y%m%d%H%M%S")
gpsd = None #seting the global variable
lncolor = "7f00ffff"
pcolor = "7f00ff00"
tlat = 0
tlong = 0
mindist = 1
dist = 0 
ttime = 0
fname = "/home/pi/gpsdata"

#Write out the kml header
fhandle = open(fname + datestmp + ".kml", "w")
fhandle.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
fhandle.write("<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n")
fhandle.write("<Document>\n")
fhandle.write("<name>Paths</name>\n")
fhandle.write("<description>Path created by gpsklogger on Raspberry Pi</description>\n")
fhandle.write("<Style id=\"yellowLineGreenPoly\">\n")
fhandle.write("    <LineStyle>\n")
fhandle.write("      <color>" + lncolor + "</color>\n")
fhandle.write("      <width>4</width>\n")
fhandle.write("   </LineStyle>\n")
fhandle.write("   <PolyStyle>\n")
fhandle.write("      <color>" + pcolor + "</color>\n")
fhandle.write("   </PolyStyle>\n")
fhandle.write("</Style>\n")
fhandle.write("<Placemark>\n")
fhandle.write("   <name>Absolute Extruded</name>\n")
fhandle.write("   <description>Transparent " + pcolor + " wall with " + lncolor + "outlines</description>\n")
fhandle.write("   <styleUrl>#yellowLineGreenPoly</styleUrl>\n")
fhandle.write("   <LineString>\n")
fhandle.write("       <extrude>1</extrude>\n")
fhandle.write("       <tessellate>1</tessellate>\n")
fhandle.write("       <altitudeMode>absolute</altitudeMode>\n")
fhandle.write("       <coordinates>\n")
fhandle.close

#clear the terminal if in debug mode (optional)
if dbug<>0:
     os.system('clear') 
 
class GpsPoller(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    global gpsd #bring it in scope
    gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
    self.current_value = None
    self.running = True #setting the thread running to true
 
  def run(self):
    global gpsd
    while gpsp.running:
      gpsd.next() #this will continue to loop and grab EACH set of gpsd info to clear the buffer
 
if __name__ == '__main__':
  gpsp = GpsPoller() # create the thread
  try:
    gpsp.start() # start it up
    while True:
      #It may take a second or two to get good data
      if gpsd.fix.longitude<>0:
          #Compute Distance between last point and this point
          dist = haversine(tlong,tlat,gpsd.fix.longitude,gpsd.fix.latitude) * 1000 
          if dist > mindist:
               #Turn on the LED
               #GPIO.output(11, True)
               #Write the longtitude, latitude and then altitude
               fhandle = open(fname + datestmp + ".kml", "a")
               fhandle.write("       "+str(gpsd.fix.longitude) + ", ")
               fhandle.write(str(gpsd.fix.latitude) + ", ")
               fhandle.write(str(gpsd.fix.altitude) + "\n")
               fhandle.close
               #Reset the position
               tlat = gpsd.fix.latitude
               tlong = gpsd.fix.longitude
               ttime = gpsd.utc
          #clear the terminal if in debug mode (optional)
          if dbug<>0:
              os.system('clear') 
              print
              print ' GPS reading'
              print '----------------------------------------'
              print '----------------------------------------'
              print 'latitude    ' , gpsd.fix.latitude
              print 'longitude   ' , gpsd.fix.longitude
              print 'time utc    ' , gpsd.utc,' + ', gpsd.fix.time
              print 'altitude (m)' , gpsd.fix.altitude
              print 'eps         ' , gpsd.fix.eps
              print 'epx         ' , gpsd.fix.epx
              print 'epv         ' , gpsd.fix.epv
              print 'ept         ' , gpsd.fix.ept
              print 'speed (m/s) ' , gpsd.fix.speed
              print 'climb       ' , gpsd.fix.climb
              print 'track       ' , gpsd.fix.track
              print 'mode        ' , gpsd.fix.mode
              print
              print 'distance    ' , dist
              print 'TTIME       ' , ttime
          
          #Take a nap for # of secs.
          #This would be a good place to look for the GPIO switch pushbutton push
          time.sleep(slpinterval)
          #GPIO.output(11, False)

  except (KeyboardInterrupt, SystemExit): #when you press ctrl+c
    if dbug<>0:
      print "\nKilling Thread..."
    fhandle = open(fname + datestmp + ".kml", "a")
    fhandle.write("       </coordinates>\n")
    fhandle.write("   </LineString>\n")
    fhandle.write("</Placemark>\n")
    fhandle.write("</Document>\n")
    fhandle.write("</kml>\n")
    fhandle.close()
    gpsp.running = False
    gpsp.join() # wait for the thread to finish what it's doing

    #Close out the LED
    GPIO.cleanup()
  print "Done.\nExiting."