import sys
import serial
import time
import struct
import signal
import RPi.GPIO as GPIO
import csv
from math import sqrt
import os
from gps import *
import time
import threading

gpsd = None #seting the global variable

GPIO.setmode(GPIO.BCM)
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)



class GpsPoller(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    global gpsd #bring it in scope
    gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
    self.current_value = None
    self.running = True #setting the thread running to true
 
  def run(self):
    global gpsd
    while gpsp.running:
      gpsd.next() #this will continue to loop and grab EACH set of gpsd info to clear the buffer



def sercmd(cmd, resp):
    done=0
    while (done==0):
        ser.write(cmd)
        time.sleep(0.1)
        data=ser.read(len(resp))
        #print data
        if (data == resp):
            done = 1
        else:
            # Send an Esc just in case it was streaming
            ser.write(chr(int('1b', 16)))
            ser.read(100)
            
    
ser = serial.Serial('/dev/ttyUSB1', 9600, timeout=0.5)


# Set binary mode
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99B\r\n', 'BINARY ON\r')
#sercmd('*99A\r\n', 'ASCII ON\r')


# Set the zero mark
sercmd('*99WE\r\n', 'OK\r')            
sercmd('*99ZF\r\n', 'ZERO OFF\r')       
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99ZN\r\n', 'ZERO ON\r')


# Set average on
#sercmd('*99WE\r\n', 'OK\r')            
#sercmd('*99VF\r\n', 'AVG OFF\r')       
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99VN\r\n', 'AVG ON\r')



# Set reset  ne marche pas
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99TN\r\n', 'S/R OFF\r')
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99TF\r\n', 'S/R ON\r')

# Set reset pulse ne marche pas
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99R\r\n', 'RST\r')
#sercmd('*99WE\r\n', 'OK\r')
#sercmd('*99S\r\n', 'SET\r')


# Set sampling rate      Set sample rate to nnn where: nnn= 10,20,25,30, 40,50, 60, 100, 123, or 154 samples/sec (default=20 sps)
sercmd('*99WE\r\n', 'OK\r')
sercmd('*99R=10\r\n', 'OK\r')

ser.write('*99C\r\n')

gpsp = GpsPoller() # create the thread
gpsp.start()
#time.sleep(0.1)


#b = open('/media/USBDRIVE/sense58.csv', 'ab')
b = open('/usr/share/adafruit/webide/repositories/my-pi-projects/Final/sense606.csv', 'ab')
a = csv.writer(b)

data1 = [['S1', 'S2', 'S3', 'TOTAL','latitude','longitude']]
a.writerows(data1)


for i in range(30000000):
    data = ser.read(7)
    mdata=struct.unpack('>hhhc', data)
    #print 's1   ', '\t', 's2   ', '\t', 's3  ', '\t','latitude   ', '\t','longitude   '
    
    s1=mdata[0]
    s2=mdata[1]
    s3=mdata[2]
    b2= (s1**2)      # carre
    c2= (s2**2)      # carre
    d2= (s3**2)      # carre
    t=b2+c2+d2
    r=sqrt(t)   #racine carre
    r2=int(round(r))
    
    print mdata[0], '\t', mdata[1], '\t', mdata[2],'\t',r2, '\t',gpsd.fix.latitude, '\t',gpsd.fix.longitude
       
    
    

            
    
    
    
    
    a = csv.writer(b)
    data = [[ mdata[0], mdata[1], mdata[2],r2, gpsd.fix.latitude, gpsd.fix.longitude]]
    a.writerows(data)

    
    
    
    
    
    
    
    input_state = GPIO.input(21)
    #print 'goio input state 24', input_state

    if input_state == False:
        print('Button shutdown Pressed')
        os.system("sudo shutdown -h now")
        time.sleep(0.2)    


# Stop streaming
ser.write(chr(int('1b', 16)))
time.sleep(0.25)
ser.read(100)
ser.close()
b.close()


